<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="style/style.css" type="text/css"> 
    <link rel="stylesheet" href="style/iphone.css" type="text/css">  
    <link rel="stylesheet" href="style/responsive.css" type="text/css"> 
    
    <title>TeleConsulta</title>
  </head>
<body>  
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">
      <img src="img/tele.png" width="250" id="tele"  class="d-inline-block align-top" alt=""> 
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="btn-direita">
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a id="fonte-cor" class="nav-item nav-link active" href="index.php">HOME <span class="sr-only">(current)</span></a>
          <a id="fonte-cor" class="nav-item nav-link" href="views/cad.php">CADASTRE-SE</a>
          <a id="fonte-cor" class="nav-item nav-link" href="views/anuncie.php">ANÚNCIE</a>
          <a id="fonte-cor" class="nav-item nav-link" href="views/blog.php?pagina=0"">BLOG</a>        
          <a id="fonte-cor" class="nav-item nav-link" href="views/quemsomos.php">QUEM_SOMOS</a>   
        </div>
      </div>
    </div>
  </nav>
  <div class="container">
    <div class="login">
      <div class="row">
        <div class="col align-self-start"></div>
          <div class="col align-self-center">
            <form method="post" action="sidebar/index.php">
              <center>
                <label for=""><h5>Acesso a página de administração</h5></label>
              </center>
              <div class="form-group">
                <label for="exampleInputEmail1">E-MAIL</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">SENHA</label>
                <input type="password" class="form-control" id="exampleInputPassword1">
              </div>
                <button type="submit" class="btn btn-primary">ENVIAR</button>
            </form>
          </div>
        <div class="col align-self-end"></div>
      </div>
    </div>
  </div> 
  <footer class="footer">
    <div class="footer-copyright text-center py-3" id="footer">© 2020 Copyright:
      <a href="" id="footer">Teleconsulta</a>
    </div>
  </footer>
</body>
</html>