<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>TeleConsulta</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" crossorigin="anonymous">

    <link rel="stylesheet" href="../style/style.css" type="text/css"> 
    <link rel="stylesheet" href="../style/iphone.css" type="text/css">  
    <link rel="stylesheet" href="../style/responsive.css" type="text/css"> 

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style.css">
</head>

<body>

         <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

 <!-- Trigger the modal with a button -->
  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-contento">
      <div class="modal-header">
        <img src="../img/tele.png" width="250">
      </div>
      <div class="modal-body">       
       
    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary">Enviar</button>
      </div>
    </div>
    </div>
  </div>
   
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <img src="../img/tele.png" width="250">
            </div>

            <ul class="list-unstyled components">
              
                <li>
                    <a href="">Cadastrar Empresa</a>
                </li>
                <li>
                    <a href="#">Cadastro usuário</a>
                </li>
                <li>
                    <a href="#"> Check Mensagens</a>
                  
                </li>
                <li>
                    <a href="#">Fazer uma postagem</a>
                </li>
              
            </ul>

          
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-danger">
                        <i class="fas fa-align-left"></i>
                        <span>Fechar sidebar</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" id="cor" href="#">Cadastrar_Empresa</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="cor" href="#">Cadastrar_Usuário</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="cor" href="#">Check_Mensagens</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="cor" href="#">Postagem</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>



          <form>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputEmail4">Nome Completo</label>
                <input type="email" class="form-control" id="inputEmail4" placeholder="Digite aqui...">
              </div>
              <div class="form-group col-md-6">
                <label for="email">E-MAIL</label>
                <input type="email" class="form-control" placeholder="Digite aqui...">
              </div>
            </div>
             <div class="form-row">
            <div class="form-group col-md-8">

              <label for="inputAddress2">Endereço</label>
              <input type="text" class="form-control" id="inputAddress2" placeholder="Digite aqui...">
            </div>

            <div class="form-group col-md-4">
                
              <label for="inputAddress2">Plano de saúde</label>
              <input type="text" class="form-control" id="inputAddress2" placeholder="Digite aqui...">
            </div>
        </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputCity">Cidade</label>
                <input type="text" class="form-control" id="inputCity" placeholder="Digite aqui...">
              </div>
              <div class="form-group col-md-2">
                <label for="inputState">Estado</label>
                <select id="inputState" class="form-control">
                  <option selected>Choose...</option>
                  <option>...</option>
                </select>
              </div>
              <div class="form-group col-md-4">
                <label for="inputZip">CEP</label>
                <input type="text" class="form-control" id="inputZip" placeholder="Digite aqui...">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="inputCity">Telefone</label>
                <input type="text" class="form-control" id="inputCity" placeholder="Digite aqui...">
              </div>
            <div class="form-group col-md-4">
              <label for="inputCity">Whatsapp Business</label>
              <input type="text" class="form-control" id="inputCity" placeholder="Digite aqui...">
            </div>
            <div class="form-group col-md-4">
              <label for="inputZip">CRM</label>
              <input type="text" class="form-control" id="inputZip" placeholder="Digite aqui...">
            </div>           
          </div>        
  <button type="submit" class="btn btn-success">CADASTRAR</button>
   <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal">PESQUISAR</button>
</form>
 
 
 
 <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" crossorigin="anonymous"></script>

       
    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>

</html>